import {Post} from './post'
import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {
  post:Post;
   @Output() deleteEvent = new EventEmitter<Post>();
    @Output() editEvent = new EventEmitter<Post[]>();
 tempPost:Post = {author:null,content:null,title:null};
  isEdit : boolean = false;
  editButtonText = 'Edit';
   sendDelete(){
    this.deleteEvent.emit(this.post);
  }
  

  
cancelEdit(){
  this.isEdit = false;
   this.post.author = this.tempPost.author;
   this.post.content = this.tempPost.content;
   this.post.title = this.tempPost.title;
   this.editButtonText = 'Edit'; 
    }
  constructor() { }
 
  toggleEdit(){
     //update parent about the change
    this.isEdit = !this.isEdit; 
    this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit'; 
     if(this.isEdit){
     this.tempPost.author = this.post.author;
     this.tempPost.content = this.post.content;
     this.tempPost.title = this.post.title;
     } else {
      let originalAndNew = [];
      originalAndNew.push(this.tempPost,this.post);
       this.editEvent.emit(originalAndNew);
     }   
    }

  ngOnInit() {
  }

}
