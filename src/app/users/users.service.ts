import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class UsersService {
 /* users = [
    {name:'John',email:'john@gmail.com'},
    {name:'Jack',email:'jack@gmail.com'},
    {name:'Alice',email:'alice@yahoo.com'}
  ] 
  getUsers(){
		return this.users;
	} */

  private _url = "http://jsonplaceholder.typicode.com/users";

  constructor(private _http: Http) { }

  getUsers(){
		//return this.users;
		return this._http.get(this._url)
			.map(res => res.json()).delay(2000)
    }
}