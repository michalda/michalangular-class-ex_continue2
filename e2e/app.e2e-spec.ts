import { MichalangularPage } from './app.po';

describe('michalangular App', function() {
  let page: MichalangularPage;

  beforeEach(() => {
    page = new MichalangularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
